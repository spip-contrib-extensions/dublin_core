<?php

// S&eacute;curit&eacute;
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'configurer_dublin_core' => 'Configurer les champs Dublin Core des articles',
	'dublin_core' => 'Dublin Core',
	'item_auteurs' => 'Auteurs de l\'article',
	'item_chapo' => 'Chapo de l\'article',
	'item_descriptif' => 'Descriptif de l\'article',
	'item_groupe_mot' => 'Mots-cl&eacute;s d\'un groupe sp&eacute;cifique',
	'item_introduction' => 'Introduction de l\'article',
	'item_motscles' => 'Tous les mots-cl&eacute;s de l\'article',
	'item_nom_site_spip' => 'Nom du site SPIP',
	'item_soustitre' => 'Sous-titre de l\'article',
	'item_surtitre' => 'Sur-titre de l\'article',
	'label_dc_creator' => 'Auteurs (DC.Creator)',
	'label_dc_description_abstract' => 'R&eacute;sum&eacute; (DC.Description .Abstract)',
	'label_dc_rights_license' => 'URL Licence (DC.Rights.License) (optionnel)',
	'label_dc_rights_license_details' => 'URL d&eacute;crivant la licence s\'appliquant &agrave; l\'article. Si vous activez le plugin Licence, la licence attribu&eacute;e &agrave; un article sera prioritaire sur la valeur saisie ici.',
	'label_dc_rights_rightsholder' => 'Propri&eacute;taire des droits (DC.Rights.RightsHolder)',
	'label_dc_subject' => 'Sujet ou mots-cl&eacute;s (DC.Subject)',
	'label_dc_subject_groupe_mot' => 'Si mots-cl&eacute;s d\'un groupe sp&eacute;cifique, lequel&nbsp;?',
	'label_issn' => 'ISSN (optionnel)',
);

?>
